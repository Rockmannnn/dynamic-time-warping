# DTW_project

## Description
This project aims to implement the dynamic time warping algorithm for analysis of two time series sequences.
Link to the descrpition of Dynamic Time Warping:\
https://en.wikipedia.org/wiki/Dynamic_time_warping

## Input
Two time series given in a list

## Output
Computed matrix with the marked optimal path 

Distance between the two time series

## Library versions

Python - 3.9.5
colorama - 0.4.4