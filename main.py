from colorama import Fore

'''function to print given cells of matrix in appropriate layout'''


def result_printing(result, optimal_path):
    i_index = 0
    for i in result:
        i_index = i_index + 1
        j_index = 0

        for j in i:
            j_index = j_index + 1
            # print((i_index, j_index))
            if j != float('inf'):
                if (i_index - 1, j_index - 1) in optimal_path:
                    if j >= 10.0:
                        print(Fore.GREEN, j, '    ', end='')
                    else:
                        print(Fore.GREEN, j, '     ', end='')
                else:
                    if j >= 10.0:
                        print(Fore.WHITE, j, '    ', end='')
                    else:
                        print(Fore.WHITE, j, '     ', end='')
            else:
                print(Fore.WHITE, j, '   ', end='')
        print()


'''function to compute dynamic time warping matrix of two given series'''


def compute_dynamic_time_warping_distance(first_sequence, second_sequence):
    DTW_array = [[0.0] * len(second_sequence) for i in range(len(first_sequence))]

    for i in range(len(first_sequence)):
        for j in range(len(second_sequence)):
            DTW_array[i][j] = float('inf')

    DTW_array[0][0] = 0

    for i in range(1, len(first_sequence)):
        for j in range(1, len(second_sequence)):
            calculate_cost = abs(first_sequence[i] - second_sequence[j])

            DTW_array[i][j] = calculate_cost + min(
                DTW_array[i - 1][j],
                DTW_array[i][j - 1],
                DTW_array[i - 1][j - 1]
            )

    return DTW_array, DTW_array[len(first_sequence) - 1][len(second_sequence) - 1]


'''function to copmute the optimal path '''


def compute_optimal_path(matrix):
    optimal_numb_index = []
    col = len(matrix)
    row = len(matrix[0])

    while (col > 2 or row > 2):

        # position of the cell from matrix [TOP LEFT, TOP, LEFT]
        direction_array = [matrix[col - 2][row - 2], matrix[col - 1][row - 2], matrix[col - 2][row - 1]]
        minimal_dist = min(matrix[col - 2][row - 2], matrix[col - 1][row - 2], matrix[col - 2][row - 1])

        if (minimal_dist == direction_array[0]):
            # print('Top LEFT')
            # print(row-1, col-1, matrix[col-2][row-2])
            optimal_numb_index.append((col - 2, row - 2))
            row = row - 1
            col = col - 1
        elif (minimal_dist == direction_array[1]):
            # print('LEFT')
            # print(row-1, col-1, matrix[col-1][row-2])
            optimal_numb_index.append((col - 1, row - 2))
            row = row - 1
        else:
            # print('TOP')
            # print(row-1, col-2, matrix[col-2][row-1])
            optimal_numb_index.append((col - 2, row - 1))
            col = col - 1

    return optimal_numb_index


seq1 = [1, 2, 3, 3, 7]
seq2 = [1, 2, 2, 2, 2, 2, 2, 4]

result, distance = compute_dynamic_time_warping_distance(seq1, seq2)
optimal_path_ind_arr = compute_optimal_path(result)
result_printing(result, optimal_path_ind_arr)
print(optimal_path_ind_arr)
print("Distance between given time series: ", distance)
